

######## Protocol per assignar a un fitxer SVG un variable a 3 paràmetres, per poder-ho definir després al QGIS.


To create SVG symbols with modifiable fill color, stroke color and stroke width in QGIS, you should replace the style attribute from the path element with these 3 attributes:

- fill="param(fill) #FFF"
- stroke="param(outline) #000"
- stroke-width="param(outline-width) 1"


If you are using InkScape, after writing the new SVG file, edit the file and replace the entire line beggining with style:

style="fill:#00a000;fill-opacity:1;stroke:#000000;stroke-width:1;(...)"

with the following line:

fill="param(fill) #FFF" stroke="param(outline) #000" stroke-width="param(outline-width) 1"
