# -*- coding: utf-8 -*-
"""
/***************************************************************************
        begin                : nov 2018
        copyright            : (C) 2018 by Carlos López Quintanilla
        email                : carlos.lopez@psig.es
 ***************************************************************************/
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Python for to do an Action to open a website inside of QGIS
#
from PyQt5.QtCore import QUrl
from PyQt5.QtWebKitWidgets import QWebView
myWV = QWebView(None)
myWV.load(QUrl('https://www1.sedecatastro.gob.es/CYCBienInmueble/OVCListaBienes.aspx?del=[% "DELEGACIO" %]&muni=[% "MUNICIPIO" %]&rc1=[%  "MASA" || "PARCELA" %]&rc2=[% "HOJA" %]'))
myWV.show()