-- Expresión para añair un campo de otra capa que intersecta con la capa 

array_to_string(
 aggregate( 
  layer:='talls_5000',
  aggregate:='array_agg',
  expression:="idvisabs",
  filter:=intersects(($geometry),geometry(@parent))
 )
)